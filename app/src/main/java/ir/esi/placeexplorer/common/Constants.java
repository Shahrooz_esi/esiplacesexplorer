package ir.esi.placeexplorer.common;

import ir.esi.placeexplorer.common.util.DecimalByteUnit;

public class Constants {

    public static final String HTTP = "http";
    public static final int HTTP_TIMEOUT_VALUE = 10;

    public static final String Base_Url = "https://api.foursquare.com/v2/";
    public static final String API_Version = "20200807";
    public static final String CLIENT_ID = "ZNZ2PXMYGYOG4OODX1054K25YU0AKOHOOARVLPXFSS0E0OPW";
    public static final String CLIENT_SECRET = "LPTWKSCBHFRVVIWEI50KULXO4EH0GBKRFWXQ42FS4HYKW13J";


    public static final int IMAGE_DISK_CACHE_SIZE = (int) DecimalByteUnit.MEGABYTES.toBytes(50);
    public static final String DB_NAME = "ESI_Explorer_DB";
    public static final int MAX_VALID_DISTANCE = 500;
    public static final int MAX_VALID_DATE = 1;
    public static final int RESPONSE_LIMIT_SIZE = 30;

    public static String VENUE_NAME_EXTRA = "VenueNameExtra";
    public static String VENUE_ID_EXTRA = "VenueIdExtra";
}
