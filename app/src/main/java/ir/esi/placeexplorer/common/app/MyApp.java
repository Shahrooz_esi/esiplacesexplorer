package ir.esi.placeexplorer.common.app;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import ir.esi.placeexplorer.common.Constants;
import ir.esi.placeexplorer.common.app.injection.component.DaggerPlacesSearchComponent;
import ir.esi.placeexplorer.common.app.injection.component.PlacesSearchComponent;
import ir.esi.placeexplorer.common.app.injection.module.PlacesSearchModule;
import ir.esi.placeexplorer.common.location.GPSTracker;
import ir.esi.placeexplorer.database.DaoMaster;
import ir.esi.placeexplorer.database.DaoSession;
import ir.esi.placeexplorer.database.ExploreEntityDao;

public class MyApp extends Application {

    private static final String TAG = MyApp.class.getSimpleName();

    private PlacesSearchComponent component;

    private static GPSTracker gps;
    private static ExploreEntityDao dao;

    @Override
    protected void attachBaseContext(final Context base) {
        super.attachBaseContext(base);
        component().inject(this);

        gps = new GPSTracker(this);
        dao = setupDb();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public PlacesSearchComponent component() {
        if (component == null) {
            component = DaggerPlacesSearchComponent.builder().placesSearchModule(new PlacesSearchModule(this)).build();
        }
        return component;
    }

    public static ExploreEntityDao getExploreDao() {
        return dao;
    }

    private ExploreEntityDao setupDb () {
        DaoMaster.DevOpenHelper masterHelper = new DaoMaster.DevOpenHelper(this, Constants.DB_NAME, null); //create database db file if not exist
        SQLiteDatabase db = masterHelper.getWritableDatabase();  //get the created database db file
        DaoMaster master = new DaoMaster(db);//create masterDao
        DaoSession masterSession=master.newSession(); //Creates Session session
        return masterSession.getExploreEntityDao();
    }


    public static GPSTracker getGps() {
        return gps;
    }

    public static void resetGps(GPSTracker gpsTracker) {
         gps = gpsTracker;
    }

}
