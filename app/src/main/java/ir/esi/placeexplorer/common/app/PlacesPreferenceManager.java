package ir.esi.placeexplorer.common.app;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import ir.esi.placeexplorer.BuildConfig;
import ir.esi.placeexplorer.common.app.injection.qualifier.ForApplication;

@Singleton
public class PlacesPreferenceManager {

    public static final String PLACES_SEARCH_PREFERENCES_KEY = "PlaceSearch_pref";

    private final SharedPreferences sharedPreferences;

    @Inject
    public PlacesPreferenceManager(@ForApplication Context context) {
        this.sharedPreferences = context.getSharedPreferences(PLACES_SEARCH_PREFERENCES_KEY, Context.MODE_PRIVATE);
    }

}
