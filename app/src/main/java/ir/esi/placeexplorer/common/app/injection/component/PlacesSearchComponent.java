package ir.esi.placeexplorer.common.app.injection.component;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.app.PlacesPreferenceManager;
import ir.esi.placeexplorer.common.app.injection.module.PlacesSearchModule;
import ir.esi.placeexplorer.common.data.VenuesDataManager;

@Singleton
@Component(modules = {PlacesSearchModule.class})
public interface PlacesSearchComponent {

    // provides
    PlacesPreferenceManager providePlacesSearchPreferenceManager();

    VenuesDataManager provideSearchDataManager();

    Picasso providePicasso();

    // injects
    void inject(MyApp app);

}
