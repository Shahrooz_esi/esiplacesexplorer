package ir.esi.placeexplorer.common.app.injection.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ir.esi.placeexplorer.common.Constants;
import ir.esi.placeexplorer.common.app.injection.qualifier.ForApplication;
import ir.esi.placeexplorer.common.util.PlacesUtil;
import ir.esi.placeexplorer.service.PlacesApi;
import ir.esi.placeexplorer.service.network.PlacesApiRetrofit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

import static java.util.concurrent.TimeUnit.SECONDS;

@Module
public class PlacesSearchModule {

    private final int CACHE_SIZE = 5 * 1024 * 1024;

    private final Context context;
    private PlacesApi mPlacesApi;

    public PlacesSearchModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    public PlacesApi providePlaces() {
        return getPlacesInstance();
    }

    @Provides
    @Singleton
    public Picasso providePicasso(OkHttpClient client) {
        return new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return createOkHttpClient(context).build();
    }

    @NonNull
    private PlacesApi getPlacesInstance() {
        if (mPlacesApi == null) {
            mPlacesApi = new PlacesApiRetrofit(PlacesUtil.getLatLngOfUserLocation());
        }
        return mPlacesApi;
    }

    private static OkHttpClient.Builder createOkHttpClient(Context context) {
        File cacheDir = new File(context.getCacheDir(), Constants.HTTP);
        Cache cache = new Cache(cacheDir, Constants.IMAGE_DISK_CACHE_SIZE);

        return new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(Constants.HTTP_TIMEOUT_VALUE, SECONDS)
                .readTimeout(Constants.HTTP_TIMEOUT_VALUE, SECONDS)
                .writeTimeout(Constants.HTTP_TIMEOUT_VALUE, SECONDS);
    }

}
