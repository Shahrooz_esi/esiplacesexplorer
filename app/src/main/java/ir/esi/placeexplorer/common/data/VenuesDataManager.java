package ir.esi.placeexplorer.common.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import ir.esi.placeexplorer.data.model.ExploreResponse;
import ir.esi.placeexplorer.data.model.VenueResponse;
import ir.esi.placeexplorer.service.PlacesApi;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;

@Singleton
public class VenuesDataManager {

    private final PlacesApi placesApi;

    @Inject
    public VenuesDataManager(PlacesApi placesApi) {
        this.placesApi = placesApi;
    }

    public Observable<Result<ExploreResponse>> explore(int offset) {
        return placesApi.explore(offset);
    }

    public Observable<Result<VenueResponse>> getVenue(String venueId) {
        return placesApi.getVenue(venueId);
    }

}
