package ir.esi.placeexplorer.common.location;

import java.util.List;

import ir.esi.placeexplorer.data.model.ExploreItem;

public interface IExploreResponse {

    void onFetchExplore(int offset, Long dbId);

    void onDataExist(List<ExploreItem> item_list);

    void onErrorRaised(Throwable t) ;

    void noMoreData();
}
