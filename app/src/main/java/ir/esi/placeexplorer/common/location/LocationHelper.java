package ir.esi.placeexplorer.common.location;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ir.esi.placeexplorer.common.Constants;
import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.util.ExploreUtil;
import ir.esi.placeexplorer.common.util.PlacesUtil;
import ir.esi.placeexplorer.common.util.TimeUtil;
import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Group;
import ir.esi.placeexplorer.data.model.Location;
import ir.esi.placeexplorer.database.DBHelper;
import ir.esi.placeexplorer.database.ExploreEntity;

public class LocationHelper {

    public void getPlaceStatus(int offset, final IExploreResponse exploreResponse) {
        List<ExploreEntity> listEntity = new DBHelper().getFromDB();
        Long dbId = null;
        if (listEntity != null && listEntity.size() > 0) {
            for (ExploreEntity entity : listEntity) {
                Location location = new Location();
                location.setLat(entity.getLat());
                location.setLng(entity.getLng());
                double distance =  PlacesUtil.getDistanceToUserLocation(location);
                int differentDate = new TimeUtil().getDifferenceBetweenDates(new Date().getTime(), entity.getLastUpdate());

                if (distance < Constants.MAX_VALID_DISTANCE && differentDate < Constants.MAX_VALID_DATE) {
                    dbId = entity.getId();
                    Group group = ExploreUtil.parseExploreGroupFromJson(entity.getExploreJson());
                    List<ExploreItem> list = new ArrayList<>();
                    if (group != null && group.getItems().size() >= offset) {
                        if (offset >= entity.getTotalResult()) {
                            exploreResponse.noMoreData();
                            return;
                        }
                        int limit = offset + Constants.RESPONSE_LIMIT_SIZE;
                        if (limit > entity.getTotalResult()) {
                            limit = entity.getTotalResult();
                        } else if (group.getItems().size() < offset + Constants.RESPONSE_LIMIT_SIZE) {
                            limit = offset;
                            exploreResponse.onFetchExplore(offset, dbId);
                            return;
                        }

                        for (int i = offset; i < limit && i < group.getItems().size() ; i++) {
                            list.add(group.getItems().get(i));
                        }
                        exploreResponse.onDataExist(list);
                        return;
                    }
                } else if (distance < Constants.MAX_VALID_DISTANCE && differentDate > Constants.MAX_VALID_DATE) {
                    new DBHelper().deleteFromDB(entity.getId());
                }
            }
        }
        exploreResponse.onFetchExplore(offset, dbId);
    }

    public static boolean isGpsCorrectInitialized() {
        if (MyApp.getGps().getLatitude() == 0 && MyApp.getGps().getLongitude() == 0) {
            return false;
        } else {
            return true;
        }
    }

}
