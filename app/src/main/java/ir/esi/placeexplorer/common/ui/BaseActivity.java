package ir.esi.placeexplorer.common.ui;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import ir.esi.placeexplorer.R;
import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.ui.injection.component.ActivityComponent;
import ir.esi.placeexplorer.common.ui.injection.component.DaggerActivityComponent;

public abstract class BaseActivity extends AppCompatActivity {

    /*@Bind(R.id.toolbar)
    public Toolbar toolbar;*/

    protected ActivityComponent component;


    public ActivityComponent component() {
        if (component == null) {
            component = DaggerActivityComponent.builder()
                    .placesSearchComponent(((MyApp) getApplication()).component())
                    .build();
        }
        return component;
    }

    protected void displayCustomToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        TextView text = layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

}
