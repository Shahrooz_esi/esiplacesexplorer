package ir.esi.placeexplorer.common.ui.injection.component;

import dagger.Component;
import ir.esi.placeexplorer.common.app.injection.component.PlacesSearchComponent;
import ir.esi.placeexplorer.common.ui.injection.scope.ActivityScope;
import ir.esi.placeexplorer.ui.MainActivity;
import ir.esi.placeexplorer.ui.VenueDetailsActivity;

@ActivityScope
@Component(dependencies = PlacesSearchComponent.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(VenueDetailsActivity activity);


}
