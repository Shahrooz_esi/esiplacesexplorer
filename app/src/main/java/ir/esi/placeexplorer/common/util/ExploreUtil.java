package ir.esi.placeexplorer.common.util;

import com.google.gson.Gson;

import java.util.List;

import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Group;

public class ExploreUtil {

    public static String parseExploreGroupJsonToString(Group group) {
        if (group != null) {
            return new Gson().toJson(group);
        } else {
            return "";
        }
    }

    public static Group parseExploreGroupFromJson(String exploreStr) {
        if (exploreStr != null && exploreStr.length() > 0) {
            return new Gson().fromJson(exploreStr, Group.class);
        } else {
            return null;
        }
    }
    public static ExploreItem parseExploreItemStringToObj(String exploreStr) {
        if (exploreStr != null && exploreStr.length() > 0) {
            return new Gson().fromJson(exploreStr, ExploreItem.class);
        } else {
            return null;
        }
    }
}
