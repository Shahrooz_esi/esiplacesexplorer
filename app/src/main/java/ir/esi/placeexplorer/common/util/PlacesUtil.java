package ir.esi.placeexplorer.common.util;

import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.data.model.Location;

public class PlacesUtil {

    public static Float getDistanceToUserLocation(Location distanceTo) {

        if (distanceTo == null || distanceTo.getLat() == 0 || distanceTo.getLng() == 0) {
            return null;
        }

        android.location.Location startPoint = new android.location.Location("a");
        startPoint.setLatitude(MyApp.getGps().getLatitude());
        startPoint.setLongitude(MyApp.getGps().getLongitude());

        android.location.Location endPoint = new android.location.Location("b");
        endPoint.setLatitude(distanceTo.getLat());
        endPoint.setLongitude(distanceTo.getLng());

        float distance = startPoint.distanceTo(endPoint);

        return distance;
    }

    public static String getLatLngOfUserLocation() {
        return MyApp.getGps().getLatitude() + "," + MyApp.getGps().getLongitude();
    }
}
