package ir.esi.placeexplorer.common.util;

public class TimeUtil {

    public int getDifferenceBetweenDates(long date1, long date2) {
        int diffInDays = (int)( (date1 - date2)
                / (1000 * 60 * 60 * 24) );
        return diffInDays;
    }
}
