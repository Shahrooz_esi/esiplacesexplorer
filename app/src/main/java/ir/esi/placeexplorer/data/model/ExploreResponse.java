package ir.esi.placeexplorer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExploreResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("response")
    @Expose
    private ExploreResp response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public ExploreResp getResponse() {
        return response;
    }

    public void setResponse(ExploreResp response) {
        this.response = response;
    }
}