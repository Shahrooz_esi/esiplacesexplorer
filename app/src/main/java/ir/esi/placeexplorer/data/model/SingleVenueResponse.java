package ir.esi.placeexplorer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ir.esi.placeexplorer.data.model.Venue;

public class SingleVenueResponse {

    @SerializedName("venue")
    @Expose
    private Venue venue;

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }
}