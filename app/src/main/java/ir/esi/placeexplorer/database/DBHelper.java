package ir.esi.placeexplorer.database;

import org.w3c.dom.Entity;

import java.util.Date;
import java.util.List;

import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.util.ExploreUtil;
import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Group;

import static ir.esi.placeexplorer.common.util.ExploreUtil.parseExploreGroupJsonToString;

public class DBHelper {
    public List<ExploreEntity> getFromDB(){
        List<ExploreEntity> list = MyApp.getExploreDao().queryBuilder().orderDesc(ExploreEntityDao.Properties.Id).build().list();
        return list;
    }
    public ExploreEntity getFromDB(long id){
        List<ExploreEntity> listEntity = new DBHelper().getFromDB();
        ExploreEntity selectedEntity = null;
        for (ExploreEntity entity: listEntity) {
            if (entity.getId() == id) {
                selectedEntity = entity;
                break;
            }
        }
        return selectedEntity;
    }

    public void deleteFromDB(Long id){
        ExploreEntityDao dao = MyApp.getExploreDao();
        MyApp.getExploreDao().deleteByKey(id);
    }

    public void saveExplore(Group exploreGroup, int offset, int totalResult, Long dbId) {
        ExploreEntity entity = new ExploreEntity();

        if (dbId != null && dbId > 0) {
            entity = getFromDB(dbId);
            Group group = ExploreUtil.parseExploreGroupFromJson(entity.getExploreJson());
            for (ExploreItem item : exploreGroup.getItems()) {
                group.getItems().add(item);
            }
            String strExplore = parseExploreGroupJsonToString(group);
            entity.setExploreJson(strExplore);
            entity.setOffset(offset);
            entity.setTotalResult(totalResult);
            MyApp.getExploreDao().update(entity);
        } else {
            String strExplore = parseExploreGroupJsonToString(exploreGroup);
            entity.setExploreJson(strExplore);
            entity.setLastUpdate(new Date().getTime());
            entity.setLat(MyApp.getGps().getLatitude());
            entity.setLng(MyApp.getGps().getLongitude());
            entity.setOffset(offset);
            entity.setTotalResult(totalResult);
            MyApp.getExploreDao().insert(entity);
        }

    }

}
