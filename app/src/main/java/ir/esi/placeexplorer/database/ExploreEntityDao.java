package ir.esi.placeexplorer.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import javax.inject.Singleton;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import ir.esi.placeexplorer.data.model.ExploreItem;

@Singleton
public class ExploreEntityDao extends AbstractDao<ExploreEntity, Long> {

    public static final String TABLENAME = "EXPLORE_ENTITY";

    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Lat = new Property(1, Double.class, "lat", false, "LAT");
        public final static Property Lng = new Property(2, Double.class, "lng", false, "LNG");
        public final static Property ExploreJson = new Property(3, String.class, "exploreJson", false, "EXPLORE_JSON");
        public final static Property LastUpdate = new Property(4, Long.class, "lastUpdate", false, "LAST_UPDATE");
        public final static Property TotalResult = new Property(5, Integer.class, "totalResult", false, "TOTAL_RESULT");
        public final static Property Offset = new Property(6, Integer.class, "offset", false, "OFFSET");
    };

    public ExploreEntityDao(DaoConfig config) {
        super(config);
    }

    public ExploreEntityDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"EXPLORE_ENTITY\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"LAT\" REAL," + // 1: lat
                "\"LNG\" REAL," + // 2: lng
                "\"EXPLORE_JSON\" TEXT," + // 3: exploreJson
                "\"LAST_UPDATE\" INTEGER," + // 4: lastUpdate
                "\"TOTAL_RESULT\" INTEGER," + // 5: totalResult
                "\"OFFSET\" INTEGER);"); // 6: offset
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"EXPLORE_ENTITY\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ExploreEntity entity) {
        stmt.clearBindings();

        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }

        Double lat = entity.getLat();
        if (lat != null) {
            stmt.bindDouble(2, lat);
        }

        Double lng = entity.getLng();
        if (lng != null) {
            stmt.bindDouble(3, lng);
        }

        String exploreJson = entity.getExploreJson();
        if (exploreJson != null) {
            stmt.bindString(4, exploreJson);
        }

        Long lastUpdate = entity.getLastUpdate();
        if (lastUpdate != null) {
            stmt.bindLong(5, lastUpdate);
        }

        Integer totalResult = entity.getTotalResult();
        if (totalResult != null) {
            stmt.bindLong(6, totalResult);
        }

        Integer offset = entity.getOffset();
        if (offset != null) {
            stmt.bindLong(7, offset);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }

    /** @inheritdoc */
    @Override
    public ExploreEntity readEntity(Cursor cursor, int offset) {
        ExploreEntity entity = new ExploreEntity( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1), // lat
            cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2), // lng
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // exploreJson
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4), // lastUpdate
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // totalResult
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6) // offset
        );
        return entity;
    }

    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ExploreEntity entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setLat(cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1));
        entity.setLng(cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2));
        entity.setExploreJson(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setLastUpdate(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
        entity.setTotalResult(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setOffset(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
     }

    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(ExploreEntity entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }

    /** @inheritdoc */
    @Override
    public Long getKey(ExploreEntity entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }

}
