package ir.esi.placeexplorer.service;

import ir.esi.placeexplorer.data.model.ExploreResponse;
import ir.esi.placeexplorer.data.model.VenueResponse;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;

/**
 * Created by vicsonvictor on 4/21/18.
 */

public interface PlacesApi {
    int READ_TIMEOUT = 20000; //ms

    Observable<Result<ExploreResponse>> explore(int offset);

    Observable<Result<VenueResponse>> getVenue(String venueId);
}
