package ir.esi.placeexplorer.service;

import ir.esi.placeexplorer.data.model.ExploreResponse;
import ir.esi.placeexplorer.data.model.VenueResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by vicsonvictor on 4/21/18.
 */

public interface VenuesService {

    @GET("venues/explore")
    Observable<Result<ExploreResponse>> explore(@Query("offset") int offset/*, @Query("limit") int limit*/);

    @GET("venues/{venue_id}")
    Observable<Result<VenueResponse>> venue(@Path("venue_id") String venueId);

}
