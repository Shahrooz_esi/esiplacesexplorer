package ir.esi.placeexplorer.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.esi.placeexplorer.R;
import ir.esi.placeexplorer.common.app.PlacesPreferenceManager;
import ir.esi.placeexplorer.common.util.PlacesUtil;
import ir.esi.placeexplorer.data.model.Venue;

public class ExploreResultsAdapter extends RecyclerView.Adapter<ExploreResultsAdapter.VenueViewHolder> {

    private List<Venue> data = new ArrayList<>();
    private VenueListener listener;
    private PlacesPreferenceManager placesPreferenceManager;
    private Context context;
    private Picasso picasso;

    public ExploreResultsAdapter(Context context, VenueListener listener, PlacesPreferenceManager placesPreferenceManager, Picasso picasso) {
        this.context = context;
        this.placesPreferenceManager = placesPreferenceManager;
        this.listener = listener;
        this.picasso = picasso;
    }

    @Override
    public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new VenueViewHolder(inflater.inflate(R.layout.list_item_venue, parent, false));
    }

    @Override
    public void onBindViewHolder(VenueViewHolder holder, int position) {
        holder.bind(context, data.get(position), listener, picasso, placesPreferenceManager);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateData(List<Venue> batches) {
        this.data = new ArrayList<>(batches);
        notifyDataSetChanged();
    }

    public interface VenueListener {
        void onVenueItemClicked(Venue venue);
    }

    public static class VenueViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.venue_container)
        public RelativeLayout venueContainer;

        @Bind(R.id.venue_category_image)
        public ImageView venueCategoryImage;

        @Bind(R.id.venue_name)
        public TextView venueName;

        @Bind(R.id.category_name)
        public TextView categoryName;

        @Bind(R.id.distance_to_user_location)
        public TextView distanceToUserLocation;

        private View itemView;
        private String venueId;

        public VenueViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        private static void loadImage(Context context, ImageView imageView, Venue venue, Picasso picasso) {

            if (venue.getCategories().isEmpty() || venue.getCategories().get(0).getIcon() == null) {
                return;
            }

            final String imageUrl = venue.getListImgUrl();

            if (imageUrl == null) {
                imageView.setImageDrawable(context.getDrawable(android.R.drawable.stat_notify_error));
                return;
            }

            picasso.load(imageUrl).into(imageView);
        }

        public void bind(final Context context, final Venue data, final VenueListener listener, Picasso picasso, final PlacesPreferenceManager placesPreferenceManager) {

            // Venue name
            venueName.setText(data.getName());

            // Category
            if (data.getCategories().size() >= 1) {
                categoryName.setText(data.getCategories().get(0).getName());
            } else {
                categoryName.setText(context.getString(R.string.unavailable));
            }

            // distance
            String distanceToUserStr = String.format("%.2f", PlacesUtil.getDistanceToUserLocation(data.getLocation()));
            if (distanceToUserStr == null) {
                distanceToUserLocation.setText(context.getString(R.string.unavailable));
            } else {
                distanceToUserLocation.setText(distanceToUserStr + " meters");
            }

            // load category image
            loadImage(context, venueCategoryImage, data, picasso);

            venueContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onVenueItemClicked(data);
                    venueId = data.getId();
                }
            });

            itemView.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
                @Override
                public void onWindowFocusChanged(boolean hasFocus) {
                    if (hasFocus && venueId != null && venueId.equals(data.getId())) {
                        venueId = null;
                    }
                }
            });
        }

    }

}
