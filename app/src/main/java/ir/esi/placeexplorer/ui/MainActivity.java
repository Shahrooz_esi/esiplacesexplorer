package ir.esi.placeexplorer.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.esi.placeexplorer.R;
import ir.esi.placeexplorer.common.Constants;
import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.app.PlacesPreferenceManager;
import ir.esi.placeexplorer.common.location.GPSTracker;
import ir.esi.placeexplorer.common.location.IExploreResponse;
import ir.esi.placeexplorer.common.location.LocationHelper;
import ir.esi.placeexplorer.common.ui.BaseActivity;
import ir.esi.placeexplorer.common.ui.view.LoadingIndicatorView;
import ir.esi.placeexplorer.common.util.PlacesUtil;
import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Location;
import ir.esi.placeexplorer.data.model.Venue;


public class MainActivity extends BaseActivity implements VenuesContract.View, ExploreResultsAdapter.VenueListener {

    private RecyclerView.Adapter adapter;
    private ContentRecyclerAdapter contentRecyclerAdapter;

    private Location lastLocation;
    private List<Venue> venueList;

    @Bind(R.id.content_recycler_view)
    RecyclerView exploreRecyclerView;
    @Bind(R.id.content_list_recycler_view)
    RecyclerView contentRecyclerView;
    @Bind(R.id.loading_indicator)
    LoadingIndicatorView loadingIndicatorView;
    @Bind(R.id.no_results_message)
    TextView noResultsMessageText;
    @Bind(R.id.suggested_searches)
    ViewGroup suggestedSearchesContainer;
    @Bind(R.id.content_header)
    ViewGroup contentHeader;
    @Bind(R.id.tv_message)
    TextView tvMessage;

    @Inject
    Picasso picasso;
    @Inject
    VenuesPresenter venuesPresenter;
    @Inject
    PlacesPreferenceManager preferenceManager;

    private LinearLayoutManager layoutManager;

    private LinearLayoutManager contentLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        component().inject(this);

        setupResultsRecyclerView();
        setupContentResultsRecyclerView();

        displayNoResultsState(false);

    }

    @Override
    protected void onPause() {
        venuesPresenter.unBindView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        venuesPresenter.bindView(this);
        doExplore();
        //doSearch("");
    }

    private void checkPlaces() {
        GPSTracker gps = MyApp.getGps();
        if (loadingIndicatorView.getVisibility() != View.VISIBLE && LocationHelper.isGpsCorrectInitialized() && needExploreNewPlace()) {
            lastLocation = new Location();
            lastLocation.setLng(gps.getLongitude());
            lastLocation.setLat(gps.getLatitude());
            doExplore();
        }
    }

    private void displayNoResultsState(boolean show) {
        if (show) {
            noResultsMessageText.setVisibility(View.VISIBLE);
            noResultsMessageText.setText(getString(R.string.search_no_results_response));
        } else {
            noResultsMessageText.setVisibility(View.INVISIBLE);
        }
    }

    private void setupResultsRecyclerView() {

        adapter = new ExploreResultsAdapter(this, this, preferenceManager, picasso);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        exploreRecyclerView.setLayoutManager(layoutManager);
        exploreRecyclerView.setAdapter(adapter);
        exploreRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loadingIndicatorView.getVisibility() != View.VISIBLE) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            doExplore();
                        };
                    }
                }
            }
        });
    }
    private void setupContentResultsRecyclerView() {
        contentLayoutManager = new LinearLayoutManager(this);
        contentRecyclerView.setLayoutManager(contentLayoutManager);
        contentRecyclerAdapter = new ContentRecyclerAdapter(contentRecyclerView, new ArrayList<String>(), contentHeader);
        contentRecyclerView.setAdapter(contentRecyclerAdapter);
    }


    private boolean needExploreNewPlace() {
        if (venueList == null) {
            return true;
        } else if ((venueList.size() > 0) &&
                PlacesUtil.getDistanceToUserLocation(lastLocation) > Constants.MAX_VALID_DISTANCE) {
            venueList = new ArrayList<>();
            return true;
        } else {
            return false;
        }
    }

    private void doExplore() {
        if (!isLoadingVisible()) {
            displayLoadingIndicator(true);
            if (venueList == null) {
                venueList = new ArrayList<>();
            }
            new LocationHelper().getPlaceStatus(venueList.size(), new IExploreResponse() {
                @Override
                public void onFetchExplore(int offset, Long dbId) {
                    venuesPresenter.doExplore(offset, dbId);
                }

                @Override
                public void onDataExist(List<ExploreItem> item_list) {
                    onExplore(item_list);
                }

                @Override
                public void onErrorRaised(Throwable t) {
                    t.printStackTrace();
                    displayLoadingIndicator(true);
                }

                @Override
                public void noMoreData() {
                    displayCustomToast(getString(R.string.no_more_data));

                    displayLoadingIndicator(false);
                }
            });
        }
    }

    @Override
    public void onExplore(List<ExploreItem> items) {
        displayLoadingIndicator(false);
        if (venueList == null) {
            venueList = new ArrayList<>();
        }
        for (ExploreItem item : items) {
            venueList.add(item.getVenue());
        }

        if (!venueList.isEmpty()) {
            displayNoResultsState(false);
            //layoutManager.scrollToPosition(0);
            ((ExploreResultsAdapter) adapter).updateData(venueList);
        } else {
            layoutManager.scrollToPosition(0);
            ((ExploreResultsAdapter) adapter).updateData(new ArrayList<Venue>());
            displayNoResultsState(true);
        }
    }

    @Override
    public void onVenue(Venue venue) {
    }

    @Override
    public void onError() {
        displayLoadingIndicator(false);
    }

    @Override
    public void onVenueItemClicked(Venue venue) {
        Intent intent = new Intent(this, VenueDetailsActivity.class);
        intent.putExtra(Constants.VENUE_NAME_EXTRA, venue.getName());
        intent.putExtra(Constants.VENUE_ID_EXTRA, venue.getId());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
    }

    public void displayLoadingIndicator(boolean show) {
        if (show) {
            loadingIndicatorView.setVisibility(View.VISIBLE);
        } else {
            loadingIndicatorView.setVisibility(View.GONE);
        }
    }

    public boolean isLoadingVisible() {
        return loadingIndicatorView.getVisibility() == View.VISIBLE;
    }
}
