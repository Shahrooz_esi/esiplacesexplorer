/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ir.esi.placeexplorer.ui;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.esi.placeexplorer.R;
import ir.esi.placeexplorer.common.Constants;
import ir.esi.placeexplorer.common.app.MyApp;
import ir.esi.placeexplorer.common.app.PlacesPreferenceManager;
import ir.esi.placeexplorer.common.ui.BaseActivity;
import ir.esi.placeexplorer.common.ui.view.LoadingIndicatorView;
import ir.esi.placeexplorer.common.ui.view.ViewUtils;
import ir.esi.placeexplorer.data.model.Category;
import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Venue;

public class VenueDetailsActivity extends BaseActivity implements VenuesContract.View {

    @Bind(R.id.directions_image)
    public ImageView directionsImage;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.loading_indicator)
    LoadingIndicatorView loadingIndicatorView;
    @Bind(R.id.address_card_view)
    CardView addressCardView;
    @Bind(R.id.venue_categories)
    TextView venueCategories;
    @Bind(R.id.venue_url)
    TextView venueUrl;
    @Bind(R.id.addr_line_1)
    TextView addrLine1;
    @Bind(R.id.addr_line_2)
    TextView addrLine2;
    @Bind(R.id.addr_line_3)
    TextView addrLine3;
    @Bind(R.id.primary_details_card_view)
    CardView primaryDetailsCardView;
    @Bind(R.id.about_card_view)
    CardView aboutCardView;

    @Inject
    Picasso picasso;

    @Inject
    VenuesPresenter venuesPresenter;

    @Inject
    PlacesPreferenceManager preferenceManager;

    private boolean isFavorite;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);
        ButterKnife.bind(this);

        component().inject(this);

        venuesPresenter.bindView(this);

        String venueId = getIntent().getStringExtra(Constants.VENUE_ID_EXTRA);

        loadingIndicatorView.setVisibility(View.VISIBLE);
        venuesPresenter.doGetVenue(venueId);
    }

    @Override
    protected void onPause() {
        venuesPresenter.unBindView();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setViewHeight(final View view, final int viewHeight) {
        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                if (view.getHeight() != viewHeight) {
                    view.getLayoutParams().height = viewHeight;
                    view.requestLayout();
                }
                return true;
            }
        });
    }

    @Override
    public void onVenue(final Venue venue) {
        if (venue != null) {

            toolbarTitle.setText(venue.getName());

            setupVenueUrl(venue);

            if (!venue.getCategories().isEmpty()) {
                venueCategories.setText(getCategoriesString(venue.getCategories()));
            } else {
                venueCategories.setVisibility(View.GONE);
            }

            setupAddressInfo(venue);
        }
    }

    @Override
    public void onError() {
        loadingIndicatorView.setVisibility(View.INVISIBLE);
        primaryDetailsCardView.setVisibility(View.INVISIBLE);
        addressCardView.setVisibility(View.INVISIBLE);
        aboutCardView.setVisibility(View.INVISIBLE);
        displayCustomToast("Something went wrong. Please try later.");
    }

    @Override
    public void onExplore(List<ExploreItem> venues) {

    }


    private void setupAddressInfo(final Venue venue) {
        if (venue.getLocation() != null && !venue.getLocation().getFormattedAddress().isEmpty()) {
            for (int i = 0; i < venue.getLocation().getFormattedAddress().size(); i++) {
                String formattedAddress = venue.getLocation().getFormattedAddress().get(i);
                if (i == 0) {
                    addrLine1.setText(formattedAddress);
                } else if (i == 1) {
                    addrLine2.setText(formattedAddress);
                } else if (i == 2) {
                    addrLine3.setText(formattedAddress);
                }
            }
            directionsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchMapsForDirections(venue);
                }
            });

        } else {
            addressCardView.setVisibility(View.GONE);
        }
    }

    private void setupVenueUrl(final Venue venue) {
        if (venue.getUrl() != null) {
            venueUrl.setText(venue.getUrl());
            venueUrl.setPaintFlags(venueUrl.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            venueUrl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchUrl(venue.getUrl());
                }
            });
        } else {
            venueUrl.setVisibility(View.GONE);
        }
    }

    private void launchMapsForDirections(Venue venue) {
        StringBuilder staticMapImageUrlBuilder = new StringBuilder();
        staticMapImageUrlBuilder.append("https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=");
        staticMapImageUrlBuilder.append(MyApp.getGps().getLatitude() + "," + MyApp.getGps().getLongitude());
        staticMapImageUrlBuilder.append("&destination=" + String.format("%.4f", venue.getLocation().getLat()));
        staticMapImageUrlBuilder.append("," + String.format("%.4f", venue.getLocation().getLng()));
        staticMapImageUrlBuilder.append("&key=AIzaSyBJ8FpHurNJQ0oyEhxY4U1HMAZ2xF_pv9w");

        final String mapUrl = staticMapImageUrlBuilder.toString();

        launchUrl(mapUrl);
    }

    private String getCategoriesString(List<Category> categories) {
        if (categories.isEmpty()) {
            return "";
        }

        StringBuilder categoriesString = new StringBuilder();
        for (int i = 0; i < categories.size(); i++) {
            categoriesString.append(categories.get(i).getName());
            if (i != categories.size() - 1) {
                categoriesString.append(", ");
            }
        }

        return categoriesString.toString();
    }

    protected void launchUrl(String urlToLoad) {

        if (ViewUtils.isChromeTabSupported(this)) { // Chrome Custom tab supported

            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            builder.setToolbarColor(ContextCompat.getColor(this, R.color.primary_dark));
            builder.setShowTitle(true);
            customTabsIntent.launchUrl(this, Uri.parse(urlToLoad));

        } else {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlToLoad));
            browserIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(browserIntent);
        }
    }

}
