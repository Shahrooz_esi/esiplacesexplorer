package ir.esi.placeexplorer.ui;

import java.util.List;

import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.Venue;

public interface VenuesContract {

    interface View {
        void onExplore(List<ExploreItem> venues);
        void onVenue(Venue venue);
        void onError();
    }

    interface Presenter {
        void bindView(VenuesContract.View view);
        void unBindView();
        void doExplore(int offset,  Long dbId);
        void doGetVenue(String venueId);
    }
}

