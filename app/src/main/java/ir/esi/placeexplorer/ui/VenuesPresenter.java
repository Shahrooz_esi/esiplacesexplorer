package ir.esi.placeexplorer.ui;

import java.util.List;

import javax.inject.Inject;

import ir.esi.placeexplorer.common.data.VenuesDataManager;
import ir.esi.placeexplorer.common.ui.injection.scope.ActivityScope;
import ir.esi.placeexplorer.data.model.ExploreItem;
import ir.esi.placeexplorer.data.model.ExploreResponse;
import ir.esi.placeexplorer.data.model.VenueResponse;
import ir.esi.placeexplorer.database.DBHelper;
import retrofit2.adapter.rxjava.Result;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@ActivityScope
public class VenuesPresenter implements VenuesContract.Presenter {

    private final CompositeSubscription subscriptions = new CompositeSubscription();
    private final VenuesDataManager venuesDataManager;
    private VenuesContract.View viewListener;

    @Inject
    public VenuesPresenter(VenuesDataManager venuesDataManager) {
        this.venuesDataManager = venuesDataManager;
    }

    @Override
    public void bindView(VenuesContract.View viewListener) {
        this.viewListener = viewListener;
    }

    @Override
    public void unBindView() {
        unsubscribe();
        this.viewListener = null;
    }

    @Override
    public void doExplore(int offset, Long dbId) {
        subscriptions.add(getExploreSubscription(offset, dbId));
    }

    @Override
    public void doGetVenue(String venueId) {
        subscriptions.add(getVenueSubscription(venueId));
    }

    private void unsubscribe() {
        subscriptions.clear();
    }

    private Subscription getVenueSubscription(String venueId) {
        return venuesDataManager.getVenue(venueId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new VenueSubscriber(viewListener));
    }

    private Subscription getExploreSubscription(int offset, Long dbId) {
        return venuesDataManager.explore(offset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ExploreSubscriber(viewListener, offset, dbId));
    }

    private static class VenueSubscriber extends Subscriber<Result<VenueResponse>> {

        private VenuesContract.View listener;

        public VenueSubscriber(VenuesContract.View listener) {
            this.listener = listener;
        }

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            listener.onError();
        }

        @Override
        public void onNext(Result<VenueResponse> result) {
            VenueResponse venueResponse = result.response().body();
            if (venueResponse != null && venueResponse.getSingleVenueResponse() != null) {
                if (listener != null) {
                    listener.onVenue(venueResponse.getSingleVenueResponse().getVenue());
                }
            }
        }
    }

    private static class ExploreSubscriber extends Subscriber<Result<ExploreResponse>> {

        private VenuesContract.View listener;
        private int offset;
        private  Long dbId;

        public ExploreSubscriber(VenuesContract.View listener, int offset,  Long dbId) {
            this.listener = listener;
            this.offset = offset;
            this.dbId = dbId;
        }

        @Override
        public void onCompleted() {
            // n/a
        }

        @Override
        public void onError(Throwable e) {
            listener.onError();
        }

        @Override
        public void onNext(Result<ExploreResponse> result) {
            ExploreResponse resp = result.response().body();
            if (resp != null && resp.getResponse().getGroups().size() > 0 && resp.getResponse().getGroups().get(0).getItems() != null
                    && resp.getResponse().getGroups().get(0).getItems().size() > 0) {
                DBHelper dbHelper = new DBHelper();
                dbHelper.saveExplore(resp.getResponse().getGroups().get(0), offset, resp.getResponse().getTotalResults(), dbId);
                List<ExploreItem> item_list = resp.getResponse().getGroups().get(0).getItems();
                if (listener != null) {
                    listener.onExplore(item_list);
                }
            }
        }
    }
}
